//
//  BDBadgeCell.m
//  Mass image to PDF converter
//
//  Created by Simon on 16/09/12.
//  Copyright (c) 2012 Simonics. All rights reserved.
//

#import "BDBadgeCell.h"

// Subclass of NSTextFieldCell
@implementation BDBadgeCell

// Initialize badge variables, based on Apple Mail.
static int BADGE_BUFFER_LEFT = 4;
static int BADGE_BUFFER_SIDE = 3;
static int BADGE_BUFFER_TOP = 1;
static int BADGE_BUFFER_LEFT_SMALL = 2;
static int BADGE_CIRCLE_BUFFER_RIGHT = 5;
static int BADGE_TEXT_HEIGHT = 14;
static int BADGE_X_RADIUS = 7;
static int BADGE_Y_RADIUS = 8;
static int BADGE_TEXT_MINI = 8;
static int BADGE_TEXT_SMALL = 18;

/*- (void)setBadgeCount:(int)newBadgeCount
{
    badgecount = newBadgeCount;
}*/

- (NSColor *)highlightColorInView:(NSView *)controlView
{
    return [NSColor clearColor];
}

- (void)drawInteriorWithFrame:(NSRect)aRect inView:(NSView *)controlView
{
    // Set up badge string and size.
    NSString *badge = [NSString stringWithFormat:@"%d", [self intValue]];//badgecount];
    NSSize badgeNumSize = [badge sizeWithAttributes:nil];
    NSFont *badgeFont = [NSFont fontWithName:@"Helvetica-Bold" size:11];
    
    // Calculate the badge's coordinates.
    int badgeWidth = badgeNumSize.width + BADGE_BUFFER_SIDE * 2;
    if (badgeNumSize.width < BADGE_TEXT_MINI)
    {
        // The text is too short. Decrease the badge's size.
        badgeWidth = BADGE_TEXT_SMALL;
    }
    int badgeX = aRect.origin.x + aRect.size.width - BADGE_CIRCLE_BUFFER_RIGHT - badgeWidth;
    int badgeNumX = badgeX + BADGE_BUFFER_LEFT;
    int badgeY = aRect.origin.y + BADGE_BUFFER_TOP;
    if (badgeNumSize.width < BADGE_TEXT_MINI)
    {
        badgeNumX += BADGE_BUFFER_LEFT_SMALL;;
    }
    NSRect badgeRect = NSMakeRect(badgeX, badgeY, badgeWidth, BADGE_TEXT_HEIGHT);
    
    // Draw the badge and number.
    NSBezierPath *badgePath = [NSBezierPath bezierPathWithRoundedRect:badgeRect xRadius:BADGE_X_RADIUS yRadius:BADGE_Y_RADIUS];
    BOOL isWindowFront = [[NSApp mainWindow] isVisible];
    BOOL isViewInFocus = [[[[self controlView] window] firstResponder] isEqual:[self controlView]];
    BOOL isCellHighlighted = [self isHighlighted];
    
    if (isWindowFront && isViewInFocus && isCellHighlighted)
    {
        [[NSColor whiteColor] set];
        [badgePath fill];
        NSDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:badgeFont forKey:NSFontAttributeName];
        [dict setValue:[NSColor alternateSelectedControlColor] forKey:NSForegroundColorAttributeName];
        [badge drawAtPoint:NSMakePoint(badgeNumX,badgeY) withAttributes:dict];
    }
    else if (isWindowFront && isViewInFocus && !isCellHighlighted)
    {
        [[NSColor colorWithCalibratedRed:.53 green:.60 blue:.74 alpha:1.0] set];
        [badgePath fill];
        NSDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:badgeFont forKey:NSFontAttributeName];
        [dict setValue:[NSColor whiteColor] forKey:NSForegroundColorAttributeName];
        [badge drawAtPoint:NSMakePoint(badgeNumX,badgeY) withAttributes:dict];
    }
    else if (isWindowFront && isCellHighlighted)
    {
        [[NSColor whiteColor] set];
        [badgePath fill];
        NSDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:badgeFont forKey:NSFontAttributeName];
        [dict setValue:[NSColor colorWithCalibratedRed:.51 green:.58 blue:.72 alpha:1.0] forKey:NSForegroundColorAttributeName];
        [badge drawAtPoint:NSMakePoint(badgeNumX,badgeY) withAttributes:dict];
    }
    else if (!isWindowFront && isCellHighlighted)
    {
        [[NSColor whiteColor] set];
        [badgePath fill];
        NSDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:badgeFont forKey:NSFontAttributeName];
        [dict setValue:[NSColor disabledControlTextColor] forKey:NSForegroundColorAttributeName];
        [badge drawAtPoint:NSMakePoint(badgeNumX,badgeY) withAttributes:dict];
    }
    else
    {
        [[NSColor disabledControlTextColor] set];
        [badgePath fill];
        NSDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:badgeFont forKey:NSFontAttributeName];
        [dict setValue:[NSColor whiteColor] forKey:NSForegroundColorAttributeName];
        [badge drawAtPoint:NSMakePoint(badgeNumX,badgeY) withAttributes:dict];
    }
}

@end