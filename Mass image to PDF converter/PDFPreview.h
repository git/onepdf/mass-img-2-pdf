//
//  PDFPreview.h
//  Mass image to PDF converter
//
//  Created by Simon on 30/08/12.
//  Copyright (c) 2012 Simonics. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <PDFKit/PDFKit.h>

@interface PDFPreview : NSWindowController

@property (unsafe_unretained) IBOutlet PDFView  *PDFPreviewView;
@property (unsafe_unretained) PDFDocument       *thePDF;

- (id)initWithPDF:(PDFDocument *)pdfDoc;
- (void)setPDF:(PDFDocument *)pdfDoc;

@end
