//
//  PDFPreview.m
//  Mass image to PDF converter
//
//  Created by Simon on 30/08/12.
//  Copyright (c) 2012 Simonics. All rights reserved.
//

#import "PDFPreview.h"

#define kMinOutlineViewSplit	90.0f

@interface PDFPreview ()

@end

@implementation PDFPreview
@synthesize PDFPreviewView, thePDF;

- (id)initWithPDF:(PDFDocument *)pdfDoc {
    self = [super initWithWindow:[self window]];
    if (self) {
        thePDF = pdfDoc;
    }
    
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    [PDFPreviewView setDocument:thePDF];
    //[[self window] ]
    [[self window] makeKeyAndOrderFront:self];
}

- (void)setPDF:(PDFDocument *)pdfDoc {
    thePDF = pdfDoc;
    [PDFPreviewView setDocument:thePDF];
    [[self window] makeKeyAndOrderFront:self];
}

#pragma mark - Split View Delegate
// I ripped this bit from an Apple demo application :p

// -------------------------------------------------------------------------------
//	splitView:constrainMinCoordinate:
//
//	What you really have to do to set the minimum size of both subviews to kMinOutlineViewSplit points.
// -------------------------------------------------------------------------------
- (CGFloat)splitView:(NSSplitView *)splitView constrainMinCoordinate:(CGFloat)proposedCoordinate ofSubviewAt:(int)index
{
	return proposedCoordinate + kMinOutlineViewSplit;
}

// -------------------------------------------------------------------------------
//	splitView:constrainMaxCoordinate:
// -------------------------------------------------------------------------------
- (CGFloat)splitView:(NSSplitView *)splitView constrainMaxCoordinate:(CGFloat)proposedCoordinate ofSubviewAt:(int)index
{
	return proposedCoordinate - kMinOutlineViewSplit;
}

// -------------------------------------------------------------------------------
//	splitView:resizeSubviewsWithOldSize:
//
//	Keep the left split pane from resizing as the user moves the divider line.
// -------------------------------------------------------------------------------
- (void)splitView:(NSSplitView *)sender resizeSubviewsWithOldSize:(NSSize)oldSize
{
	NSRect newFrame = [sender frame]; // get the new size of the whole splitView
	NSView *left = [[sender subviews] objectAtIndex:0];
	NSRect leftFrame = [left frame];
	NSView *right = [[sender subviews] objectAtIndex:1];
	NSRect rightFrame = [right frame];
    
	CGFloat dividerThickness = [sender dividerThickness];
    
	leftFrame.size.height = newFrame.size.height;
    
	rightFrame.size.width = newFrame.size.width - leftFrame.size.width - dividerThickness;
	rightFrame.size.height = newFrame.size.height;
	rightFrame.origin.x = leftFrame.size.width + dividerThickness;
    
	[left setFrame:leftFrame];
	[right setFrame:rightFrame];
}

@end
