//
//  droppableScrollView.m
//  Mass image to PDF converter
//
//  Created by Simon on 8/10/12.
//  Copyright (c) 2012 Simonics. All rights reserved.
//

#import "droppableScrollView.h"

@implementation droppableScrollView

- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender {
    NSPasteboard *pboard;
    NSDragOperation sourceDragMask;
    
    sourceDragMask = [sender draggingSourceOperationMask];
    pboard = [sender draggingPasteboard];
    
    if ( [[pboard types] containsObject:NSColorPboardType] ) {
        if (sourceDragMask & NSDragOperationGeneric) {
            return NSDragOperationGeneric;
        }
    }
    
    if ( [[pboard types] containsObject:NSFilenamesPboardType] ) {
        if (sourceDragMask & NSDragOperationLink) {
            return NSDragOperationLink;
        } else if (sourceDragMask & NSDragOperationCopy) {
            return NSDragOperationCopy;
        }
    }
    return NSDragOperationNone;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender {
    NSPasteboard *pboard;
    NSDragOperation sourceDragMask;
    
    sourceDragMask = [sender draggingSourceOperationMask];
    pboard = [sender draggingPasteboard];
    CFStringRef pasteboard = (CFStringRef) CFBridgingRetain(pboard);
    CFStringRef fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassNSPboardType, pasteboard, NULL);
    if (UTTypeConformsTo(fileUTI, kUTTypeDirectory)) {
        //NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
        
    }
    return YES;
}

@end
