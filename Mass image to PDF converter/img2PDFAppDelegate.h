//
//  img2PDFAppDelegate.h
//  Mass image to PDF coverter
//
//  Created by Simon on 11/08/12.
//  Copyright (c) 2012 Simonics. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <PDFKit/PDFKit.h>

#import "PDFPreview.h"

@interface img2PDFAppDelegate : NSObject <NSApplicationDelegate> {
    NSThread *folderProcThread;
}

@property (assign) IBOutlet NSWindow *window; // The window
//@property (unsafe_unretained) IBOutlet NSDrawer *optionsDrawer;
@property (unsafe_unretained) IBOutlet NSView *windowView; // The windows' view
@property (unsafe_unretained) IBOutlet NSArrayController *arrayController; // Controls the Table View
@property (unsafe_unretained) IBOutlet NSTableView *theTable; // The actual table view
@property (unsafe_unretained) IBOutlet NSPopUpButton *output; // The output selection popup button
// Progress sheet
@property (unsafe_unretained) IBOutlet NSPanel *theSheet;
@property (unsafe_unretained) IBOutlet NSProgressIndicator *progress;

@property (strong) NSMutableArray *previewControllers;
@property (strong) PDFPreview *previewWindow;

@property (unsafe_unretained) IBOutlet NSButtonCell *singlePDFOption;
@property (unsafe_unretained) IBOutlet NSButtonCell *multiplePDFOption;
@property (unsafe_unretained) IBOutlet NSImageView *previewImageWell;

- (IBAction)processFolders:(id)sender; // Start the processing of folders
-(void)folderProc; // The actual processing happens here
@property (retain) NSThread *folderProcThread;
- (IBAction)cancelProcess:(id)sender; // Cancel the processing

- (IBAction)previewPDFs:(id)sender; // Preview selected PDFs

- (void)addToTable:(NSString*)folderPath; // Adds to the table view
- (IBAction)removeFromTable:(id)sender; // Removes selected row from the array controller, and thus the table view

- (IBAction)doOpen:(id)sender; // Choose folder


@end
