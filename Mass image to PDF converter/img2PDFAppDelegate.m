//
//  img2PDFAppDelegate.m
//  Mass image to PDF coverter
//
//  Created by Simon on 11/08/12.
//  Copyright (c) 2012 Simonics. All rights reserved.
//

#import "img2PDFAppDelegate.h"

@implementation img2PDFAppDelegate
@synthesize window = _window; // The window
//@synthesize optionsDrawer;
@synthesize windowView; // The windows' view

@synthesize theSheet, progress; // Progress sheet
@synthesize theTable, arrayController; // The table and its controller
@synthesize singlePDFOption, multiplePDFOption; // The options for output
@synthesize output; // The output popup button
@synthesize folderProcThread; // The thread that processes the images

@synthesize previewControllers, previewWindow; // Stuff that manages the preview window
@synthesize previewImageWell; // The small preview image well

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication*)sender {
	return YES;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    //[optionsDrawer open:self];
    previewControllers = [previewControllers initWithArray:nil];
    [self doOpen:self];
}

- (IBAction)doOpen:(id)sender {
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setCanChooseDirectories:YES];
    [openPanel setCanCreateDirectories:YES];
    [openPanel setPrompt:@"Add to list"]; // Should be localized, but I can't be bothered
    [openPanel setMessage:@"Choose some folders to add to the list..."]; // Should also be localized, but I can't be bothered
    [openPanel setCanChooseFiles:NO];
    [openPanel setAllowsMultipleSelection:YES];
    
    NSInteger result = [openPanel runModal];
    if(result == NSOKButton){
        for (NSURL*dirURL in [openPanel URLs]){
            [self addToTable:[dirURL path]];
        }
    }
    
}

- (void)addToTable:(NSString *)folderPath {
    NSMutableDictionary *value = [[NSMutableDictionary alloc] init];
    // Add some values to the dictionary
    // which match up to the NSTableView bindings
    [value setObject:folderPath forKey:@"path"];
    NSArray *dirContents = [[NSFileManager defaultManager]
                            contentsOfDirectoryAtPath:folderPath error:nil];
    NSInteger inc = 0;
    for (NSString *fileName in dirContents) {
        CFStringRef fileExtension = (CFStringRef) CFBridgingRetain([fileName pathExtension]);
        CFStringRef fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, NULL);
        if (UTTypeConformsTo(fileUTI, kUTTypeImage)) {
            /*if(inc==0) {
                NSImage *image = [[NSImage alloc] initWithContentsOfFile:
                                  [NSString stringWithFormat:@"%@/%@",folderPath,fileName]];
                [previewImageWell setImage:image];
            }*/
            inc++;
        }
        CFRelease(fileUTI);
    }
    
    [value setObject:[NSNumber numberWithLong:inc] forKey:@"numberOfImages"];
    [arrayController addObject:value];
    [theTable reloadData];
}

- (IBAction)removeFromTable:(id)sender {
    [arrayController removeObjectsAtArrangedObjectIndexes:[theTable selectedRowIndexes]]; //selectedRow]];
    [theTable reloadData];
}

- (IBAction)showProgressSheet:(id)sender {
    [NSApp beginSheet:theSheet
       modalForWindow:(NSWindow *)_window
        modalDelegate:self
       didEndSelector:nil
          contextInfo:nil];
}

-(IBAction)hideProgressSheet:(id)sender {
    [NSApp endSheet:theSheet];
    [theSheet orderOut:sender];
}

#pragma mark The processing

-(IBAction)processFolders:(id)sender {
    folderProcThread = [[NSThread alloc] initWithTarget:self selector:@selector(folderProc) object:nil];
    [folderProcThread start];
}

-(void)folderProc {
    //Set up progress indicator
    [progress setMaxValue:([[arrayController content] count]*2)];
    [progress setDoubleValue:0];
    [self showProgressSheet:self];
    
    // if single PDF option is on, this is used
    PDFDocument *mergedPDF = [[PDFDocument alloc] init];
    PDFOutline *mergedOutline = [[PDFOutline alloc] init];
    [mergedPDF setOutlineRoot:mergedOutline];
    
    for (NSDictionary*val in [arrayController content]) {
        NSString *path = [val objectForKey:@"path"];
        
        PDFDocument *pdf = [[PDFDocument alloc] init];
        
        NSArray *dirContents = [[NSFileManager defaultManager]
                                contentsOfDirectoryAtPath:path error:nil];
        
        // if single PDF option is on, this is used
        PDFDestination *dest = [[PDFDestination alloc] initWithPage:[mergedPDF pageAtIndex:[mergedPDF pageCount]-1] atPoint:NSMakePoint(0,0)];
        
        for (NSString *fileName in dirContents) {
            CFStringRef fileExtension = (CFStringRef) CFBridgingRetain([fileName pathExtension]);
            CFStringRef fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, NULL);
            if (UTTypeConformsTo(fileUTI, kUTTypeImage)) {
                NSImage *image = [[NSImage alloc] initWithContentsOfFile:
                                  [NSString stringWithFormat:@"%@/%@",path,fileName]];
                PDFPage *page = [[PDFPage alloc] initWithImage:image];
                
                if ([multiplePDFOption state]== NSOnState) {
                    [pdf insertPage:page atIndex: [pdf pageCount]];
                } else { // if single PDF option is on, this is used
                    [mergedPDF insertPage:page atIndex: [mergedPDF pageCount]];
                }
            }
            CFRelease(fileUTI);
        }
        [progress incrementBy:1];
        
        if ([multiplePDFOption state]== NSOnState) {
            switch ([output indexOfSelectedItem]) {
                case 0: { // Save inside each directory
                    [pdf writeToFile:[NSString stringWithFormat:@"%@/%@.pdf",path,[path lastPathComponent]]];
                    break;
                }
                case 1: { // Save inside directory parents
                    [pdf writeToFile:[NSString stringWithFormat:@"%@.pdf",path]];
                    break;
                }
                case 2: { // Save to the Desktop
                    NSString *dirToOutput = [NSString stringWithFormat:@"%@/Desktop/PDF Output", NSHomeDirectory()];
                    NSFileManager *fileManager= [NSFileManager defaultManager];
                    if(![fileManager fileExistsAtPath:dirToOutput])
                        if(![fileManager createDirectoryAtPath:dirToOutput withIntermediateDirectories:YES attributes:nil error:NULL])
                            NSLog(@"Error: Create folder failed %@", dirToOutput);
                    [pdf writeToFile:[NSString stringWithFormat:@"%@/%@.pdf",dirToOutput,[path lastPathComponent]]];
                    break;
                }
                default: { // Error
                    NSLog(@"How did you select an option that doesn't exist?");
                    break;
                }
            }
        } else { // Single PDF, add outline
            PDFOutline *bookmark = [[PDFOutline alloc] init];
            [bookmark setLabel:[path lastPathComponent]];
            [bookmark setDestination:dest];
            [mergedOutline insertChild:bookmark atIndex:[mergedOutline numberOfChildren]];
        }
        [progress incrementBy:1];
    }
    if ([singlePDFOption state]== NSOnState) {
        [mergedPDF writeToFile:[NSString stringWithFormat:@"%@/Desktop/Output.pdf", NSHomeDirectory()]];
    }
    [self hideProgressSheet:self];
}

- (IBAction)cancelProcess:(id)sender {
    [folderProcThread cancel];
    [self hideProgressSheet:self];
}

#pragma mark Preview function

- (IBAction)previewPDFs:(id)sender {
    NSLog(@"%@",[[arrayController content] objectsAtIndexes:[theTable selectedRowIndexes]]);
    for (NSDictionary*val in [[arrayController content] objectsAtIndexes:[theTable selectedRowIndexes]]) {
        NSString *path = [val objectForKey:@"path"];
        
        PDFDocument *pdf = [[PDFDocument alloc] init];
        
        NSArray *dirContents = [[NSFileManager defaultManager]
                                contentsOfDirectoryAtPath:path error:nil];
        
        for (NSString *fileName in dirContents) {
            CFStringRef fileExtension = (CFStringRef) CFBridgingRetain([fileName pathExtension]);
            CFStringRef fileUTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, NULL);
            if (UTTypeConformsTo(fileUTI, kUTTypeImage)) {
                NSImage *image = [[NSImage alloc] initWithContentsOfFile:
                                  [NSString stringWithFormat:@"%@/%@",path,fileName]];
                PDFPage *page = [[PDFPage alloc] initWithImage:image];
                [pdf insertPage:page atIndex: [pdf pageCount]];
            }
            CFRelease(fileUTI);
        }
        previewWindow = [[PDFPreview alloc] initWithWindowNibName:@"PDFPreview"];
        [previewWindow setPDF:pdf];
        [previewWindow showWindow: self];
        [previewControllers addObject:previewWindow];
    }
}

@end
