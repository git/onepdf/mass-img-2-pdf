//
//  main.m
//  Mass image to PDF coverter
//
//  Created by Simon on 11/08/12.
//  Copyright (c) 2012 Simonics. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
